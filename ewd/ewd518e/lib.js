function loadXMLDoc(filename) {
  xhttp=new XMLHttpRequest();
  xhttp.open("GET",filename,false);
  xhttp.send();
  var xmlDoc = xhttp.responseXML;
  var responseString = (new XMLSerializer()).serializeToString(xmlDoc);
  responseString = responseString.replace(/>\s*/g, '>').replace(/\s*</g, '<');
  var parser = new DOMParser();
  return parser.parseFromString(responseString,"text/xml");
}

function getCurrentPDFName()
{
	if( parent != null ){
		return (parent.getPDFName());
	}
	else{
		alert( "error occured" );
	}
}

function getFileName(strURL)
{
	var regExp = /\/|\\/;
	var str = strURL.split( regExp );
	return(str[str.length-1]);
}

function cutExtension( fileName )
{
	var pos = fileName.lastIndexOf(".");
	if( pos < 0 )	return fileName;

	return fileName.slice( 0, pos );
}
